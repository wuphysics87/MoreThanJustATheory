\contentsline {schapter}{Introduction}{v}{chapter*.1}
\contentsline {chapter}{\numberline {1}A Few Mathematical Prerequisites}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Algebra}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Trigonometry}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Vectors}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Differential Calculus}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Integral Calculus}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Coulomb's Law: The Force Between Charges}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Experimentally Establishing Coulomb's Law}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Coulomb's Law Breakdown}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Units and Scaling}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}A Curious Parallel}{12}{section.2.4}
\contentsline {section}{\numberline {2.5}The Superposition Principle}{12}{section.2.5}
\contentsline {section}{\numberline {2.6}Distributions of Charge}{14}{section.2.6}
\contentsline {section}{\numberline {2.7}Summary}{20}{section.2.7}
\contentsline {chapter}{\numberline {3}The Electric Field}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}The Definition of the Electric Field}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Reversing Our Definition}{22}{section.3.2}
\contentsline {section}{\numberline {3.3}Electric Field produced by distributions of charge and a new $dl$}{24}{section.3.3}
\contentsline {section}{\numberline {3.4}Summary}{25}{section.3.4}
\contentsline {chapter}{\numberline {4}The Electric Potential}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}A Review of the Dot Product}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Revisiting Some Old Friends}{28}{section.4.2}
\contentsline {section}{\numberline {4.3}This Shows Potential}{29}{section.4.3}
\contentsline {section}{\numberline {4.4}Point Charges and A Geometrical Interpretation}{31}{section.4.4}
\contentsline {section}{\numberline {4.5}Summary}{32}{section.4.5}
\contentsline {chapter}{\numberline {5}Flux and an Introduction to Gauss's Law}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}An Intruduction to Flux}{35}{section.5.1}
\contentsline {section}{\numberline {5.2}Captain Gauss: The First Maxwell Equation}{37}{section.5.2}
\contentsline {section}{\numberline {5.3}Summary}{38}{section.5.3}
\contentsline {chapter}{\numberline {6}Symmetries and Applications of Gauss's Law}{39}{chapter.6}
\contentsline {section}{\numberline {6.1}Picking a Surface Based on the Right Symmetry}{39}{section.6.1}
\contentsline {section}{\numberline {6.2}Three Types of Symmetry}{40}{section.6.2}
\contentsline {section}{\numberline {6.3}Conductors, Insulators, and Evaluating $q_{in}$}{42}{section.6.3}
\contentsline {section}{\numberline {6.4}Summary}{46}{section.6.4}
\contentsline {chapter}{\numberline {7}Circuit Components}{47}{chapter.7}
\contentsline {section}{\numberline {7.1}Capacitors}{47}{section.7.1}
\contentsline {section}{\numberline {7.2}Resistors}{50}{section.7.2}
\contentsline {chapter}{\numberline {8}Time Independent Circuits}{53}{chapter.8}
\contentsline {section}{\numberline {8.1}A Little Bit of Review}{53}{section.8.1}
\contentsline {section}{\numberline {8.2}Part One of Faraday's Law}{54}{section.8.2}
\contentsline {section}{\numberline {8.3}Current Continuity}{55}{section.8.3}
\contentsline {section}{\numberline {8.4}Steps for Circuit Analysis}{55}{section.8.4}
\contentsline {section}{\numberline {8.5}Summary}{59}{section.8.5}
\contentsline {chapter}{\numberline {9}Magnetic Fields}{61}{chapter.9}
\contentsline {section}{\numberline {9.1}The Cross Product}{61}{section.9.1}
\contentsline {section}{\numberline {9.2}The Lorentz Force Law}{62}{section.9.2}
\contentsline {section}{\numberline {9.3}The Biot Savart Law}{63}{section.9.3}
\contentsline {section}{\numberline {9.4}Ampere's Law}{65}{section.9.4}
\contentsline {section}{\numberline {9.5}Summary}{68}{section.9.5}
\contentsline {chapter}{\numberline {10}Faraday's Law}{69}{chapter.10}
\contentsline {section}{\numberline {10.1}The Cross Product Again}{70}{section.10.1}
\contentsline {section}{\numberline {10.2}Faraday's Law Breakdown}{71}{section.10.2}
\contentsline {section}{\numberline {10.3}Lenz's Law}{71}{section.10.3}
\contentsline {section}{\numberline {10.4}Applications of Faraday's Law}{72}{section.10.4}
\contentsline {section}{\numberline {10.5}Summary}{74}{section.10.5}
\contentsline {chapter}{\numberline {11}Inductance and Time Dependent Circuits}{75}{chapter.11}
\contentsline {section}{\numberline {11.1}Getting Inductance Out of Faraday's Law}{75}{section.11.1}
\contentsline {section}{\numberline {11.2}Our First Time Dependent Circuit}{76}{section.11.2}
\contentsline {section}{\numberline {11.3}A Lot of Fancy Words: Solutions to Linear First Order Inhomogenous Differential Equations}{78}{section.11.3}
\contentsline {section}{\numberline {11.4}Analogs In Mechanics: Grandma's Porch Door}{79}{section.11.4}
\contentsline {section}{\numberline {11.5}Summary}{80}{section.11.5}
\contentsline {chapter}{\numberline {12}Maxwell's Equation and the Wave Equation}{81}{chapter.12}
\contentsline {section}{\numberline {12.1}How Maxwell Saved Ampere's Law}{81}{section.12.1}
\contentsline {section}{\numberline {12.2}Maxwell's Equations}{82}{section.12.2}
\contentsline {section}{\numberline {12.3}The Wave Equation}{83}{section.12.3}
\contentsline {section}{\numberline {12.4}Maxwell's Equations in Differential Form}{86}{section.12.4}
\contentsline {section}{\numberline {12.5}Summary}{87}{section.12.5}
\contentsline {chapter}{\numberline {13}Epilogue}{89}{chapter.13}
